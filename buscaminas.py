# -*- coding: utf-8 -*-
import os
import platform
import random
import re
import string
import sys

STR_INPUT_MAIN_MENU = "Pulse enter para volver al menú principal..."
STR_INPUT_FILE_NAME = "Ingrese el nombre del archivo: "
STR_DOT_SAL_FILE_EXT_FORMAT = "{}.sal"

def set_spaces(index, value_list):
    """
       1 digit:  2 spaces between character
       2 digits; 1 space between character
    """
    # check if index > 1 and check if value_list isNumber
    # then returns the data with 1 space.
    # else, when value_list is character such as '.' return character
    # with 2 spaces
    if len(str(index)) > 1 and isinstance(value_list, int):
        return '{} '.format(value_list)
    else:
        return '{}  '.format(value_list)

def generate_rows(size_list):
    """
       We generate a list with the letters of the alphabet
       Max length = 26
       Min length = 1

       example:
       >>> generate_rows(3)
       ['A', 'B', 'C']
       >>> generate_rows(0)
       []
    """
    return list(string.ascii_uppercase[0:size_list]) if size_list > 0 and size_list < 27 else []

def generate_columns(size_list):
    """
      We generate a list with columns 
      Max length = 26
      Min length = 1

      example:
      >>> generate_columns(26)
      ['  ',1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
      >>> generate_columns(0)
      []
    """
    columns = list()
    for number in range(0, size_list +1):
        if size_list > 0 and size_list < 27:
            columns.append(set_spaces(number, number))
    if columns:
        columns[0] = '   '
    return columns

def clear_screen():
    """
     Check OS: If your OS is Ms Windows the command for clear screen is cls, but if your OS is
               Apple OSX or any Linux Kernel Distro then the command is clear
    """
    command = ''
    if 'Windows' in platform.system():
        command = 'cls'
    else:
        command = 'clear'
    return os.system(command)

def make_board(row, column, character):
    board = []
    for y in range(row):
        board.append([])
        for x in range(column):
            board[y].append(set_spaces(x, character))
    return board

def draw_board(row, column, board):
    row_alphabet = generate_rows(row)
    columns = generate_columns(column)
    print(''.join(columns))
    for index, row_board in enumerate(board):
        print("{}  {}".format(row_alphabet[index], ''.join(row_board)))

def difficulty_calculation(row, column, difficulty):
    percentage = 0

    if difficulty == 'F':
        percentage = 0.10 # 10%
    elif difficulty == 'M':
        percentage = 0.15 # 15%
    elif difficulty == 'D':
        percentage = 0.20 # 20%
    else: # Obviously is X
        percentage = 0.25 # 25%

    return (row * column) * percentage

def map_of_mines(size, number_of_mines):
    min_value = 0
    # this is the first elemen is the size of matrix
    total_list = []
    letters = generate_rows(size)
    while True:
        number = random.randint(min_value, size-1)
        letter = random.choice(letters)
        code = '{}{}'.format(letter, number)
        if len(total_list) < number_of_mines:
            # Validation if list is not empty
            if total_list:
                # Don't repeat random codes
                if not code in total_list:
                    total_list.append(code)
            else:
                total_list.append(code)
        else:
            break
    total_list.insert(0, str(size))
    return total_list

def get_coordinates(size, coordinates):
    """
    Return x and y coordinates
    example 
    A1 = 1 -> x , A(0) -> y
    """
    # split numbers and letters from mines_list
    regexp = re.compile("([A-Z]+)([0-9]+)")
    letters = generate_rows(size)
    try:
        y, x = regexp.match(coordinates).groups()
        return int(x), letters.index(y)
    except AttributeError:
        print("Formato inválido")
        return None, None

def put_mines(size, board, mines, mines_list):
    temp_mines_list = [get_coordinates(size, coordinates) for coordinates in mines_list] # noqa
    for mine in temp_mines_list:
        # pattern matching obtain x and y coordinates
        x, y = mine
        if board[y][x - 1] != '*  ':
            board[y][x - 1] = '*  '
    return board

def put_signals(row, column, board):
    for y in range(row):
        for x in range(column):
            if board[y][x] == '*  ':
                for i in [-1, 0, 1]:
                    for j in [-1, 0, 1]:
                        if 0 <= y+i <= row-1 and 0 <= x+j <= column-1 and board[y+i][x+j]!='*  ': # noqa
                            board[y+i][x+j] = set_spaces(int(board[y+i][x+j]) +1, int(board[y+i][x+j]) +1) # noqa
    return board

def fill_board(hide_board, board, y, x, row, column, value):
    zeros_coordinates = [(y, x)]
    while len(zeros_coordinates) > 0:
        y, x = zeros_coordinates.pop()
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if 0 <= y+i <= row-1 and 0 <= x+j <= column-1:
                    if board[y+i][x+j] == value and hide_board[y+i][x+j] == '0  ': # noqa
                        board[y+i][x+j] = '0  '
                        if (y+i, x+j) not in zeros_coordinates:
                            zeros_coordinates.append((y+i, x+j))
                    else:
                        board[y+i][x+j] = hide_board[y+i][x+j]
    return board

def board_is_filled(board, hide_board, row, columnm, value):
    for y in range(row):
        for x in range(columnm):
            if board[y][x] == value and hide_board[y][x] != '*  ':
                return False
    return True

def show_mines(board, hide_board, row, column):
    for y in range(row):
        for x in range(column):
            if board[y][x] == '.  ' and hide_board[y][x] == '*  ':
                board[y][x] = hide_board[y][x]
    return board

def generate_board():
    min_range = 5
    max_range = 26
    difficulty_list = ['F', 'M', 'D', 'X']
    try:
        file_board_txt = str(input(STR_INPUT_FILE_NAME)).lower()
    except (KeyboardInterrupt, EOFError):
        print("Bye!")
        sys.exit(0)
    file_board_sal = ''

    # A simple validation when you forget put extension
    if not file_board_txt.find(".txt") > 0:
        file_board_sal = STR_DOT_SAL_FILE_EXT_FORMAT.format(file_board_txt)
        file_board_txt = "{}.txt".format(file_board_txt)
    else:
        file_board_sal = STR_DOT_SAL_FILE_EXT_FORMAT.format(file_board_txt.split('.')[0])

    random_row_column = random.randint(min_range, max_range)
    difficulty_choiced = random.choice(difficulty_list)

    # We create the txt file with size board and difficulty
    with open(file_board_txt, 'a+') as txtfile:
        for item in [random_row_column, difficulty_choiced]:
            txtfile.writelines(str(item) + '\n')
        txtfile.seek(0)
        txtfile.close()

    # Get difficulty and convert to integer
    total_mines = int(difficulty_calculation(random_row_column, random_row_column, difficulty_choiced)) # noqa
    mines_list = map_of_mines(random_row_column, total_mines)

    # Creation of .Sal file
    with open(file_board_sal, 'a+') as salfile:
        for item in mines_list:
            salfile.writelines(str(item) + '\n')
        salfile.seek(0)
        salfile.close()

    # Archivo generado exitosamente
    print("Se han generado los siguientes archivos: ")
    print("> ",file_board_txt)
    print("> ",file_board_sal)
    input(STR_INPUT_MAIN_MENU)
    clear_screen()

def load_board():
    try:
        file_board_sal = str(input(STR_INPUT_FILE_NAME)).lower()
    except (KeyboardInterrupt, EOFError):
        print("Bye!")
        sys.exit(0)
    # A simple validation when you forget put extension
    if not file_board_sal.find(".sal") > 0:
        file_board_sal = STR_DOT_SAL_FILE_EXT_FORMAT.format(file_board_sal)
    coordinates_list = []

    try:
        with open(file_board_sal, 'r') as salfile:
            # obtain the data and remove \n into the list
            coordinates_list = list(map(lambda s: s.strip(), salfile.readlines())) # noqa
    except FileNotFoundError:
        print("El archivo {} no existe".format(file_board_sal))
        input(STR_INPUT_MAIN_MENU)
        clear_screen()
        return

    row = column = int(coordinates_list[0])
    board = make_board(row, column, '.')
    hide_board = make_board(row, column, '0')
    hide_board = put_mines(row, hide_board, len(coordinates_list[1:]), coordinates_list[1:]) # noqa
    # Colocamos las pistas
    hide_board = put_signals(row, column, hide_board)

    clear_screen()
    print()
    draw_board(row, column, board)
    print()

    # Play the game
    play = True

    while play:
        print("Ingresa la casilla del tablero que deseas abrir")  # noqa
        try:
            option = str(input("> ")).upper()
        except (KeyboardInterrupt, EOFError):
            print("Bye!")
            sys.exit(0)
        x, y = get_coordinates(row, option)
        if x is not None and y is not None and x <= column and y <= row and x > 0:
            x-=1
            if hide_board[y][x] == '*  ':
                board[y][x] = '*  '
                play = False
            elif hide_board[y][x] != '0  ':
                board[y][x] = hide_board[y][x]
            elif hide_board[y][x] == '0  ':
                board[y][x] = '0  '
                board = fill_board(hide_board, board, y, x, row, column, '.  ')

        clear_screen()
        print()
        draw_board(row, column, board)
        print()

        win = False
        if board_is_filled(board, hide_board, row, column, '.  '):
            win = True
            play = False

    if win:
        print("¡Felicidades Ganaste!")

    else:
        print("¡Lo sentimos Perdiste!")
        input("Pulse enter para revelar las minas...")
        clear_screen()
        board = show_mines(board, hide_board, row, column)
        print()
        draw_board(row, column, board)
        print()

    input(STR_INPUT_MAIN_MENU)
    clear_screen()

def check_option(option_menu):
    if '1' in option_menu:
        generate_board()
    elif '2' in option_menu:
        load_board()
    else:
        # Finally Exit the Game
        print("Bye!")
        # Successfully Exit
        sys.exit(0)

def make_menu():
    while True:
        print("Escoge una opción: (1) Generar Tablero (2) Cargar Tablero (3) Salir")  # noqa
        try:
            option = str(input("> "))
        except (KeyboardInterrupt, EOFError):
            print("Bye!")
            sys.exit(0)
        if option in ['1', '2', '3']:
            check_option(option)
        else:
            clear_screen()


if __name__ == '__main__':
    make_menu()
